# Kezbek Backend Engine

Welcome to the Kezbek Engine. This Backend application is built using Spring Boot as the main framework and use microservices architecture. The purpose of this application is to allow partner to process cashback with sophisticated rules & customer will receive cashback from purchasing items from partner online shop.

## Main Feature

Main feature of this backend is processing, calculating, and notify partner & customer after customer checkout using promo code from the partner online shop. After transaction from online shop, partner send request to backend and, the Kezbek system do the rest, below the detail

- Processing cashback from partner transaction
- Update Promo Rule, coupon code, date availability
- Register partner
- Secure Apps using JWT token between partner → kezbek engine
- Secure Apps using api-key and using kong to manage api gw
- Improvement Next Feature (to do)
    - CMS for partner to manage customer
    - Top Up Balance
    - API to manage Rule

## Architecure
![Text](https://gitlab.com/agusprg/kezbek-system/kzbk-backend/-/raw/main/public/kezbek.jpg)

**Microservices** are a way of structuring a software application as a set of independently deployable services. This architecture can have several benefits:

- Scalability: Because each microservice is a self-contained unit, it is easier to scale specific services.
- Modularity: By decomposing a monolithic application into smaller services, it becomes easier to understand and maintain the overall system.
- Resilience: Because each microservice is independent, the failure of one service does not necessarily affect the others.

**Why using Kong,**
Kong is an open-source API gateway that can be used to manage, secure, and extend microservices. Some of the benefits of using Kong as an API gateway include:

- Traffic management: Kong can be used to route traffic between microservices and to manage traffic levels to prevent overloading.
- Security: Kong can be configured to authenticate and authorize API requests and to enforce security policies such as rate limiting and IP whitelisting.
- Monitoring: Kong provides detailed logs and metrics that can be used to monitor and troubleshoot issues with the microservices.
- Extensibility: Kong can be extended with plugins to add additional functionality, such as transformation of API requests and responses or integration with third-party services.

## **Tech Stack**

- Spring Boot with multimodule service
- Kong API Gateway handling routing & security
- RabbitMQ for messaging queue
- Mailjet API for sending email
- Drools for rule engine processing
- Swagger for API documentation
- PostgreSQL with spring data JPA

## **Tech Database**
Actualy the database is not normally well, the design is flat except the rule related data, so in the future the design can fit using any database technology no exception with nosql db, thanks for sping data jpa with the magic repository, we can easily move to another database tech

![Text](https://gitlab.com/agusprg/kezbek-system/kzbk-backend/-/raw/main/public/erd.png)


## **How to Run**

To run the application, please clone this repo and run mvn clean install
Alternatively can download docker-compose completely to run docker image from gitlab docker-hub
    
1. test-local zero dependency docker-compose setup
    https://gitlab.com/agusprg/kezbek-system/kzbk-infra/-/tree/main/test-local
2. Production grade docker-compose setup 
    https://gitlab.com/agusprg/kezbek-system/kzbk-infra

## **API Documentation**

API documentation is available through Swagger at **`[http://localhost:8080/swagger-ui.html]`** (http://localhost:8080/swagger-ui.html.at) each service

For Agregate documentation you can check at the url :

- [Postman Api Docs](https://documenter.getpostman.com/view/811569/2s8Z75SpYk)

## **Email Notifications**

![Email](https://gitlab.com/agusprg/kezbek-system/kzbk-backend/-/raw/main/public/email.png)

The application utilizes the Mailjet API to send email notifications to customers when they have earned cashback.

## **Rule Processing**

The application utilizes Drools for rule engine processing to determine the amount of cashback a customer will receive. The rule saved on database, so partner can easily update the rule.

## **CI CD Pipeline**

![cicd](https://gitlab.com/agusprg/kezbek-system/kzbk-backend/-/raw/main/public/ci-cd.png)

This repo has implemented the ci cd pipeline to simplify testing & deployment to env development
below is the job that will run once there is a change in the code

1. **Maven Build** : Test build the whole code
2. **Testing** : a unit test provided by junit, with total at least 22 test cases, including Logic cashback, loyalty level, validation request response
3. **Dockerize** : Build docker container ready to deploy
4. **Deploy-dev** : deploy to development server, in this case jobs deploy to oracle cloud free tier & server credential saved to secret variable

## **Contact**

For any questions or issues, please contact agusdwi89@gmail.com. We hope you find this application useful in calculating your cashback from purchasing items from online shops!
