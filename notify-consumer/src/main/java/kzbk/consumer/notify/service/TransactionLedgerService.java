package kzbk.consumer.notify.service;

import kzbk.common.entity.Partner;
import kzbk.common.entity.TransactionLedger;
import kzbk.common.enums.TransactionLedgerType;
import kzbk.common.helper.exception.BusinessException;
import kzbk.common.repository.PartnerRepository;
import kzbk.common.repository.TransactionLedgerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class TransactionLedgerService {

    @Autowired
    TransactionLedgerRepository repository;

    @Autowired
    PartnerRepository partnerRepository;

    @Transactional
    public Boolean setDeduct(String partnerCode, Long total,String customerEmail, String customerEmoney){
        Partner partner = partnerRepository.findByPartnerCode(partnerCode).orElseThrow(BusinessException::notFound);

        Long before = partner.getBalance();
        Long after = before - total;

        if(after < 0 ) return false;
        repository.save(
                TransactionLedger.builder()
                        .balance_before(before)
                        .balance_after(after)
                        .value(total)
                        .email(customerEmail)
                        .emoneyNumber(customerEmoney)
                        .partnerCode(partnerCode)
                        .transactionType(TransactionLedgerType.DEDUCT)
                        .createdAt(new Date())
                        .build());
        partner.setBalance(after);
        partnerRepository.save(partner);
        return true;
    }
}
