package kzbk.consumer.notify.service;

import kzbk.common.dto.response.TransactionCashbackResponse;
import kzbk.common.entity.CashbackTransaction;
import kzbk.common.entity.Customer;
import kzbk.common.entity.Partner;
import kzbk.common.enums.TrxCashbackStatus;
import kzbk.consumer.notify.config.MailJetConfig;
import kzbk.consumer.notify.dto.MailJetRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class EmailService {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    MailJetConfig mailJetConfig;

    public void sendEmail(TransactionCashbackResponse trx, Customer customer,Partner partner, CashbackTransaction ct){
        MailJetRequest mailJetRequest = getEmailBody(trx,customer,partner, ct);
        HttpHeaders headers = generateMailJetHeader();

        Map<String, Object> outterMap = new HashMap<>();
        outterMap.put("Messages",mailJetRequest.getMessages());

        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(outterMap,headers);
        String url = mailJetConfig.getHost();
        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(url).build(true);

        restTemplate.exchange(uriComponents.toUri(), HttpMethod.POST, entity, Map.class).getBody();
    }

    private HttpHeaders generateMailJetHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBasicAuth(mailJetConfig.getUsername(),mailJetConfig.getPassword());
        return headers;
    }

    private MailJetRequest getEmailBody(TransactionCashbackResponse trx, Customer customer, Partner partner, CashbackTransaction ct) {
        List<MailJetRequest.Email> emailList = new ArrayList<>();
        MailJetRequest.Email emailTo = MailJetRequest.Email.builder()
                .email(trx.getEmail())
                .name(customer.getName())
                .build();
        emailList.add(emailTo);

        MailJetRequest.MessagesBody body = MailJetRequest.MessagesBody.builder()
                .From(MailJetRequest.Email.builder()
                        .email(mailJetConfig.getFrom())
                        .name(mailJetConfig.getDescription())
                        .build())
                .To(emailList)
                .TemplateID(mailJetConfig.getTemplateid())
                .TemplateLanguage(true)
                .Subject("You Get Kezbek "+trx.getTransactionId()+" from "+partner.getPartnerName())
                .Variables(MailJetRequest.VariableItems.builder()
                        .partner(partner.getPartnerName())
                        .name(customer.getName())
                        .trxid(trx.getTransactionId() +" ("+ct.getLevel()+")")
                        .cid(trx.getId())
                        .totalqty(numericFormat(Math.toIntExact(trx.getTotal())) + " / " + trx.getQty())
                        .totalcashback(numericFormat(Math.toIntExact(trx.getCashback().getTotalCashback())))
                        .wallet(customer.getEmoneyWallet().toString())
                        .build())
                .build();
        List<MailJetRequest.MessagesBody> lb = new ArrayList<>();
        lb.add(body);

        return MailJetRequest.builder()
                .Messages(lb)
                .build();
    }

    private String numericFormat(Integer number){
        log.info(number.toString());
        String formatted = "Rp. "+String.format("%,d", number);
        return formatted.replace(",",".");
    }
}