package kzbk.consumer.notify.service;

import kzbk.common.dto.response.TransactionCashbackResponse;
import kzbk.common.entity.CashbackTransaction;
import kzbk.common.entity.Customer;
import kzbk.common.entity.CustomerPartnerId;
import kzbk.common.entity.Partner;
import kzbk.common.helper.exception.BusinessException;
import kzbk.common.repository.CashbackTransactionRepository;
import kzbk.common.repository.CustomerRepository;
import kzbk.common.repository.PartnerRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RabbitMQConsumer {

    private final CashbackTransactionRepository ctr;
    private final TransactionLedgerService transactionLedgerService;
    private final CustomerRepository customerRepository;
    private final RestTemplate restTemplate;
    private static ExecutorService executorService = Executors.newCachedThreadPool();
    private final PartnerRepository partnerRepository;
    private final EmailService emailService;

    @Value("${data.test.emoneycashback}")
    String URL_CASHBACK;

    @RabbitListener(queues = {"${spring.rabbitmq.transaction-queue}"})
    public void consumeTransactionQueue(TransactionCashbackResponse message){
        log.info(String.format("Received message -> %s", message));
        Optional<CashbackTransaction> ct =
                ctr.findByTransactionIdAndEmoneyTransfer(message.getTransactionId(),false);
        if(ct.isPresent()){
            CashbackTransaction ctGet = ct.get();
            Customer customer = customerRepository.findById(
                    new CustomerPartnerId(ctGet.getPartnerId(), message.getEmail())).get();

            Boolean transactionStatus =
                    transactionLedgerService.setDeduct(ctGet.getPartnerId(),ctGet.getTotalCashback(),
                            customer.getEmail(),customer.getEmoneyNumber());
            if(!transactionStatus)
                throw BusinessException.balanceInsuficient();

            sendToEmoneyWallet(customer,ctGet);
            ctGet.setEmoneyTransfer(true);
            ctr.save(ctGet);
            return;
        }
    }

    private void sendToEmoneyWallet(Customer customer, CashbackTransaction ct){
        executorService.submit(()-> {
            Map<String, Object> outterMap = new HashMap<>();
            outterMap.put("emoney_number",customer.getEmoneyNumber());
            outterMap.put("value", ct.getTotalCashback());
            outterMap.put("wallet",customer.getEmoneyWallet());

            HttpEntity<Map<String, Object>> entity = new HttpEntity<>(outterMap);
            String url = URL_CASHBACK;
            UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(url).build(true);

            restTemplate.exchange(uriComponents.toUri(), HttpMethod.POST, entity, HashMap.class).getBody();
        });
    }

    @RabbitListener(queues = {"${spring.rabbitmq.notify-queue}"})
    public void consumeNotifyQueue(TransactionCashbackResponse message){
        log.info(String.format("Received message -> %s", message));
        Optional<CashbackTransaction> ct =
                ctr.findByTransactionId(message.getTransactionId());
        if(!ct.isPresent())
            return;
        CashbackTransaction ctget = ct.get();
        Customer customer = customerRepository.findById(
                new CustomerPartnerId(ctget.getPartnerId(), message.getEmail())).get();
        Partner partner = partnerRepository.findByPartnerCode(customer.getPartnerCode()).get();

        if(!ctget.isCallbackNotify()) {
            sendToCallbackPartner(customer,ctget,message,partner);
            ctget.setCallbackNotify(true);
        }

        if(!ctget.isEmailNotify()) {
            emailService.sendEmail(message,customer,partner,ctget);
            ctget.setEmailNotify(true);
        }

        ctr.save(ctget);
        return;
    }

    private void sendToCallbackPartner(Customer customer, CashbackTransaction ct, TransactionCashbackResponse cashback, Partner partner){
        Map<String, Object> outterMap = new HashMap<>();
        outterMap.put("transaction_id",ct.getTransactionId());
        outterMap.put("email", customer.getEmail());
        outterMap.put("cashback", cashback);

        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(outterMap);
        String url = partner.getCallbackUrl();
        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(url).build(true);

        restTemplate.exchange(uriComponents.toUri(), HttpMethod.POST, entity, HashMap.class).getBody();
    }

}
