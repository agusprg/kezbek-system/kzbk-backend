package kzbk.consumer.notify;

import kzbk.common.dto.response.TransactionCashbackResponse;
import kzbk.consumer.notify.config.RabbitMQpropertyConfig;
import kzbk.consumer.notify.dto.MailJetRequest;
import kzbk.consumer.notify.dto.MailJetRequest.*;
import kzbk.consumer.notify.dto.MailJetRequest.MessagesBody.*;
import kzbk.consumer.notify.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
public class testController {

    @PostMapping("mock/cashback")
    Object test(@RequestBody Object o){
        return o;
    }
}
