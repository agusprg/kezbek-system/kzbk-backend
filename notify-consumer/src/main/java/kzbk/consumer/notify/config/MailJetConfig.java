package kzbk.consumer.notify.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties("mailjet")
public class MailJetConfig {
    private String host;
    private String username;
    private String password;
    private Integer templateid;
    private String from;
    private String description;
}