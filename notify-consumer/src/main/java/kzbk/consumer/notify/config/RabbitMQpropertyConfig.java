package kzbk.consumer.notify.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties("spring.rabbitmq")
public class RabbitMQpropertyConfig {
    private String host;
    private String password;
    private String port;
    private String username;

    private String transactionExchange;
    private String transactionQueue;
    private String transactionRoutingkey;

    private String notifyExchange;
    private String notifyQueue;
    private String notifyRoutingkey;
}