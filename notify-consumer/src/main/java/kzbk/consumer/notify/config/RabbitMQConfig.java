package kzbk.consumer.notify.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Autowired
    RabbitMQpropertyConfig rbmqConf;

    @Bean
    Queue queue() {
        return new Queue(rbmqConf.getTransactionQueue(), true, false, false);
    }

    @Bean
    Queue notifyQueue() {
        return new Queue(rbmqConf.getNotifyQueue(), true, false, false);
    }

    @Bean
    Exchange myExchange() {
        return ExchangeBuilder.directExchange(rbmqConf.getTransactionExchange()).durable(true).build();
    }

    @Bean
    Exchange notifyMyExchange() {
        return ExchangeBuilder.directExchange(rbmqConf.getNotifyExchange()).durable(true).build();
    }

    @Bean
    Binding binding() {
        return BindingBuilder
                .bind(queue())
                .to(myExchange())
                .with(rbmqConf.getTransactionRoutingkey())
                .noargs();
    }

    @Bean
    Binding bindingNotify() {
        return BindingBuilder
                .bind(notifyQueue())
                .to(notifyMyExchange())
                .with(rbmqConf.getNotifyRoutingkey())
                .noargs();
    }

    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory(rbmqConf.getHost());
        cachingConnectionFactory.setUsername(rbmqConf.getUsername());
        cachingConnectionFactory.setPassword(rbmqConf.getPassword());
        return cachingConnectionFactory;
    }
    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }
}