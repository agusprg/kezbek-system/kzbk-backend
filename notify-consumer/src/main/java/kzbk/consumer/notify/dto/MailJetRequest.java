package kzbk.consumer.notify.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
public class MailJetRequest {
    List<MessagesBody> Messages;

    @Data
    @Builder
    @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
    public static class MessagesBody {
        private Integer TemplateID;
        private Boolean TemplateLanguage;
        private String Subject;

        private Email From;
        private List<Email> To;

        private VariableItems Variables;
    }

    @Data
    @Builder
    @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
    public static class Email {
        private String email;
        private String name;
    }

    @Data
    @Builder
    public static class VariableItems {
        private String partner;
        private String name;
        private String trxid;
        private String cid;
        private String totalqty;
        private String totalcashback;
        private String wallet;
    }
}