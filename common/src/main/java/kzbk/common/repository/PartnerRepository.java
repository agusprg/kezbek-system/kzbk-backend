package kzbk.common.repository;

import kzbk.common.entity.Partner;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PartnerRepository extends JpaRepository<Partner, Long> {

    Optional<Partner> findByPartnerCode(String partnerCode);

    Optional<Partner> findByEmail(String partnerEmail);
}
