package kzbk.common.repository;

import kzbk.common.entity.CashbackTransaction;
import kzbk.common.entity.Customer;
import kzbk.common.entity.CustomerPartnerId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface CashbackTransactionRepository extends JpaRepository<CashbackTransaction, String> {

    Optional<CashbackTransaction> findByTransactionId(String transactionId);

    Optional<CashbackTransaction> findByTransactionIdAndEmoneyTransfer(String transactionId,Boolean status);

    List<CashbackTransaction> findAllByEmailAndPartnerId(String email, String partnerCode);
}
