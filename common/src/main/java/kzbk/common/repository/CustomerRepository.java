package kzbk.common.repository;


import kzbk.common.entity.Customer;
import kzbk.common.entity.CustomerPartnerId;
import kzbk.common.entity.Partner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, CustomerPartnerId> {

    Optional<Customer> findById(CustomerPartnerId id);
}
