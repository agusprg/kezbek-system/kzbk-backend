package kzbk.common.repository;

import kzbk.common.entity.Partner;
import kzbk.common.entity.TransactionLedger;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TransactionLedgerRepository extends JpaRepository<TransactionLedger, Long> {
}
