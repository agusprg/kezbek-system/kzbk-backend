package kzbk.common.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import kzbk.common.enums.TransactionLedgerType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionLedger {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_TRANSACTION_LEDGER")
    @SequenceGenerator(name="SEQ_TRANSACTION_LEDGER", sequenceName = "SEQ_TRANSACTION_LEDGER", allocationSize=1, initialValue = 1)
    @Column(name = "RULE_ID")
    Long id;
    Date createdAt;

    @Enumerated(EnumType.STRING)
    TransactionLedgerType transactionType;

    String partnerCode;
    String email;
    String emoneyNumber;
    Long balance_before;
    Long value;
    Long balance_after;
}