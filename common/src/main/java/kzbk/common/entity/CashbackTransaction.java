package kzbk.common.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import kzbk.common.enums.LoyaltyGroup;
import kzbk.common.enums.TrxCashbackStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CashbackTransaction {

    @Id
    private String id;
    private String transactionId;
    private String partnerId;
    private String email;
    private LoyaltyGroup loaylty;

    @Enumerated(EnumType.STRING)
    private TrxCashbackStatus status;
    private boolean emailNotify = false;
    private boolean callbackNotify = false;
    private boolean emoneyTransfer =false;
    private Long shoppingCashback;
    private Long loyaltyCashback;
    private Long totalCashback;
    private Long total;
    private Integer qty;
    private String level;

    public Long getTotalCashback() {
        return shoppingCashback+loyaltyCashback;
    }
}