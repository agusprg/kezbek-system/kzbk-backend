package kzbk.common.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import kzbk.common.enums.EmoneyWallet;
import lombok.*;

import javax.persistence.*;
import java.util.Base64;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Partner {
    @Id
    private String partnerCode;
    private String email;
    private String partnerName;

    @Enumerated(EnumType.STRING)
    private EmoneyWallet emoneyWallet;
    private Long balance = 0L;

    private String phoneNumber;
    private String website;
    private String callbackUrl;

    @Transient
    private String apiKey;

    @Transient
    @JsonIgnore
    private String secret;

    public String getSecret() {
        return Base64.getEncoder().encodeToString(
                (email+partnerCode).getBytes());
    }
}