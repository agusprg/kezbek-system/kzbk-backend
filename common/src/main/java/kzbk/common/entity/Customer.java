package kzbk.common.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import kzbk.common.enums.EmoneyWallet;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Customer {
    @EmbeddedId
    @JsonIgnore
    private CustomerPartnerId id;

    @Transient
    private String email;

    @Transient
    private String partnerCode;

    private String name;

    @Enumerated(EnumType.STRING)
    private EmoneyWallet emoneyWallet;
    private String emoneyNumber;

    public String getEmail() {
        return id.getEmail();
    }

    public String getPartnerCode() {
        return id.getPartnerCode();
    }
}
