package kzbk.common.helper;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TransactionHelper {


    private static SecureRandom secureRandom = new SecureRandom();

    public static String generateCzbkTrxId() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmssSSS");
        return String.format("%s%s%06d",  "CZBK", sdf.format(date), secureRandom.nextInt(1000000));
    }
}
