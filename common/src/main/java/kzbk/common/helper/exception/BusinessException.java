package kzbk.common.helper.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class BusinessException extends RuntimeException {
    private String errorCode;
    private HttpStatus httpStatus;

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String errorCode, String message, HttpStatus httpStatus) {
        super(message);
        this.errorCode = errorCode;
        this.httpStatus = httpStatus;
    }

    public static BusinessException notFound() {
        return new BusinessException("400","Request not found",HttpStatus.BAD_REQUEST);
    }

    public static BusinessException dataNotFound() {
        return new BusinessException("400","Given data not found",HttpStatus.BAD_REQUEST);
    }

    public static BusinessException emailAlreadyTaken() {
        return new BusinessException("400","Email already taken please choose another one ",HttpStatus.BAD_REQUEST);
    }

    public static BusinessException invalidPartner() {
        return new BusinessException("ER-001","Invalid Partner Code",HttpStatus.BAD_REQUEST);
    }

    public static BusinessException alreadyOnBoarding() {
        return new BusinessException("ER-002","Customer already onboarding",HttpStatus.BAD_REQUEST);
    }

    public static BusinessException customerNotRegistered() {
        return new BusinessException("ER-003","Customer Not Registered",HttpStatus.BAD_REQUEST);
    }

    public static BusinessException transactionIdalreadyExist() {
        return new BusinessException("ER-004","Transaction ID already exist, please choose another one",HttpStatus.BAD_REQUEST);
    }

    public static BusinessException balanceInsuficient() {
        return new BusinessException("ER-005","insuficient balance",HttpStatus.BAD_REQUEST);
    }

    public static BusinessException generalError() {
        return new BusinessException("500","General Error ",HttpStatus.BAD_REQUEST);
    }

    public static BusinessException invalidInput() {
        return new BusinessException("400","Invalid input",HttpStatus.BAD_REQUEST);
    }

    public static BusinessException maxDayReach() {
        return new BusinessException("day range max is 60 day");
    }

    public static BusinessException alreadyExist(String item, Object id) {
        return new BusinessException(String.format("%s Already Exist: %s", item, id));
    }


}
