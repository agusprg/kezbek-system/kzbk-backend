package kzbk.common.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class HeaderExtraction {

    @Autowired
    private Environment env;
    @Value("${data.test.consumer-name}")
    private String CONSUMER_USERNAME;

    public String getPartnerId(HttpServletRequest httpRequest){
        String environment = env.getActiveProfiles()[0];
        if(environment.equals("local") || environment.equals("test") )
            return CONSUMER_USERNAME;
        else
            return httpRequest.getHeader("x-consumer-username");
    }
}
