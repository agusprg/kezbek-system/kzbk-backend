package kzbk.common.enums;

public enum TrxCashbackStatus {
    INIT,
    PROCESSING,
    COMPLETED,
    FAILED
}
