package kzbk.common.enums;

public enum LoyaltyGroup {
    BRONZE,
    SILVER,
    GOLD,
    PLATINUM
}
