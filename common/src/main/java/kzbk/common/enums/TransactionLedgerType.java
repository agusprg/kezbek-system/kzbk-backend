package kzbk.common.enums;

public enum TransactionLedgerType {
    DEDUCT,
    TOP_UP
}
