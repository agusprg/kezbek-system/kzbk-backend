package kzbk.common.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import kzbk.common.enums.TrxCashbackStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
public class TransactionCashbackResponse {

    private String id;
    private String transactionId;
    private CashbackResponse cashback;
    private String email;
    private Integer qty;
    private Long total;
    private TrxCashbackStatus status;

}
