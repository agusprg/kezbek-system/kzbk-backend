package kzbk.common.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CashbackResponse {

    private Long shoppingCashback;
    private Long loyaltyCashback;
    private Long totalCashback;
    private String level;

    public Long getTotalCashback() {
        return shoppingCashback+loyaltyCashback;
    }
}
