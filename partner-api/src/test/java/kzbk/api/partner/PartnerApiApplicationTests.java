package kzbk.api.partner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import kzbk.api.partner.controller.PartnerController;
import kzbk.api.partner.model.dto.PartnerRegistrationRequest;
import kzbk.api.partner.service.KongService;
import kzbk.api.partner.service.PartnerService;
import kzbk.common.entity.Partner;
import kzbk.common.enums.EmoneyWallet;
import kzbk.common.repository.CashbackTransactionRepository;
import kzbk.common.repository.CustomerRepository;
import kzbk.common.repository.PartnerRepository;
import kzbk.common.repository.TransactionLedgerRepository;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PartnerController.class)
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(addFilters = false)
@ActiveProfiles("test")
class PartnerApiApplicationTests {

    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    private MockMvc mockMvc;
    @MockBean
    private PartnerRepository partnerRepository;
    @MockBean
    private CustomerRepository customerRepository;
    @MockBean
    private CashbackTransactionRepository cashbackTransactionRepository;
    @MockBean
    private KongService kongService;
    @MockBean
    PartnerService partnerService;
    @MockBean
    private TransactionLedgerRepository repository;

    @Test
    void registerPartnerNonValidBody() throws Exception {
        PartnerRegistrationRequest requestBody = PartnerRegistrationRequest.builder()
                .partnerName("TOKOSHOP")
                .build();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(requestBody);

        mockMvc.perform(post("/partner/register").contentType(APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void registerPartnerValidBody() throws Exception {
        PartnerRegistrationRequest requestBody = PartnerRegistrationRequest.builder()
                .partnerName("TOKOSHOP")
                .callbackUrl("dummyurl")
                .email("agusdwi89@gmail.com")
                .emoneyWallet(EmoneyWallet.OVO)
                .phoneNumber("08111111")
                .website("www.okasodasd.com")
                .build();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(requestBody);

        mockMvc.perform(post("/partner/register").contentType(APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().isOk());
    }

    @Test
    void getPartnerInfo() throws Exception {
        mockMvc.perform(get("/partner/info")
                .contentType(APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void getTokenSecret() throws Exception {
        Mockito.when(kongService.createPartnerConsumer(new Partner())).thenReturn(new Partner());
        Partner testName = kongService.createPartnerConsumer(new Partner());
        Assert.assertEquals("bnVsbG51bGw=", testName.getSecret());
    }


}
