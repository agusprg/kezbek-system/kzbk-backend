package kzbk.api.partner.model.dto;

import kzbk.common.enums.EmoneyWallet;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class PartnerRegistrationRequest {
    @Email(message = "Email is not valid", regexp = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")
    @NotEmpty(message = "Email must be valid & can't be empty")
    private String email;

    @NotEmpty(message = "Partner Name can't empty")
    private String partnerName;

    @NotEmpty(message = "Phone Number can't empty")
    private String phoneNumber;

    @NotEmpty(message = "Website can't empty")
    private String website;

    @NotEmpty(message = "Please provide callback url for transaction result")
    private String callbackUrl;

    @NotNull(message = "Please look up value from api/lov/wallet ")
    private EmoneyWallet emoneyWallet;
}
