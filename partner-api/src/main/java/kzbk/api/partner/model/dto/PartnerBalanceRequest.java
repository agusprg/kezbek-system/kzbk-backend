package kzbk.api.partner.model.dto;

import kzbk.common.enums.EmoneyWallet;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class PartnerBalanceRequest {
    @NotEmpty(message = "Partner Name can't empty")
    private String partnerCode;

    @NotNull(message = "Balance can't empty")
    private Long balance;
}
