package kzbk.api.partner.model.dto.kong;

import lombok.Data;

@Data
public class ConsumerResponse {
    private String id;
    private String[] tags;
    private String username;
}