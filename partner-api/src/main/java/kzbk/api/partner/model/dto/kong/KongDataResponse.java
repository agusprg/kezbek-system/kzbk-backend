package kzbk.api.partner.model.dto.kong;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KongDataResponse {
    private String id;
    private String key;
    private String secret;
    private String algorithm;
}
