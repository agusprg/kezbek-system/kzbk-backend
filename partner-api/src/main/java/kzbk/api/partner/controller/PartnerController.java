package kzbk.api.partner.controller;

import io.swagger.annotations.ApiOperation;
import kzbk.api.partner.model.dto.PartnerBalanceRequest;
import kzbk.api.partner.model.dto.PartnerRegistrationRequest;
import kzbk.api.partner.model.dto.kong.KongDataResponse;
import kzbk.api.partner.service.KongService;
import kzbk.api.partner.service.PartnerService;
import kzbk.common.dto.response.BaseResponse;
import kzbk.common.entity.Partner;
import kzbk.common.helper.HeaderExtraction;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("partner")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PartnerController {

    private final PartnerService partnerService;
    private final HeaderExtraction header;

    @PostMapping("register")
    @ApiOperation(value= "Register partner", response = PartnerController.class)
    BaseResponse<Partner> registerPartner(@Valid @RequestBody PartnerRegistrationRequest partner){
        return partnerService.processRegister(partner);
    }

    @PostMapping("callback/balance")
    @ApiOperation(value= "Callback to update balance")
    BaseResponse updateBalance(@Valid @RequestBody PartnerBalanceRequest balance){
        return partnerService.processUpdateBalance(balance);
    }

    @GetMapping("/secure/info")
    @ApiOperation(value= "Partner info")
    BaseResponse<Partner> infoPartner(HttpServletRequest httpRequest){
        return partnerService.getInfo(header.getPartnerId(httpRequest));
    }

    @ApiOperation(value= "Partner info")
    @GetMapping("/secure/token")
    BaseResponse partnerNewToken(HttpServletRequest httpRequest){
        return partnerService.getToken(header.getPartnerId(httpRequest));
    }

    @GetMapping("/protected/token/refresh")
    BaseResponse partnerRefreshToken(HttpServletRequest httpRequest){
        String partnerCode = httpRequest.getHeader("x-consumer-username");
        String consumerId = httpRequest.getHeader("x-credential-identifier");

        return partnerService.getRefreshToken(partnerCode,consumerId);
    }
}