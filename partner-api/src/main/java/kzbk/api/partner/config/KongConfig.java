package kzbk.api.partner.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties("kong")
public class KongConfig {
    private String host;
    private String apiConsumer;
    private String apiConsumerKey;
    private String apiConsumerJwt;
}