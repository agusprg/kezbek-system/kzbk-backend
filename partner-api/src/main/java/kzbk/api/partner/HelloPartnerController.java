package kzbk.api.partner;

import kzbk.common.entity.Partner;
import kzbk.common.repository.PartnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class HelloPartnerController {

    @Autowired
    PartnerRepository partnerRepository;

    @GetMapping("hello")
    Object get(){
        return "hello from partner";
    }
}
