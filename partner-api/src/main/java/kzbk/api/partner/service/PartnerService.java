package kzbk.api.partner.service;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import kzbk.api.partner.model.dto.PartnerBalanceRequest;
import kzbk.api.partner.model.dto.PartnerRegistrationRequest;
import kzbk.api.partner.model.dto.kong.KongBaseResponse;
import kzbk.common.helper.ObjectMapperUtils;
import kzbk.common.helper.exception.BusinessException;
import kzbk.api.partner.model.dto.kong.KongDataResponse;
import kzbk.common.dto.response.BaseResponse;
import kzbk.common.entity.Partner;
import kzbk.common.repository.PartnerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.util.*;


@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PartnerService {

    private final PartnerRepository partnerRepository;
    private final KongService kongService;
    @Value("${jwt.kid}")
    private String JWT_KEY_ID;
    @Value("${jwt.expiry}")
    private Integer JWT_EXPIRY;


    public BaseResponse processRegister(PartnerRegistrationRequest partnerRegister){
        Optional<Partner> optPartnerEmail = partnerRepository.findByEmail(partnerRegister.getEmail());
        if(optPartnerEmail.isPresent())
            throw BusinessException.emailAlreadyTaken();

        Partner partner = ObjectMapperUtils.map(partnerRegister,Partner.class);
        partner.setPartnerCode(getRandomCode(partnerRegister.getEmail()));
        partner.setApiKey(getRandomApiKey());

        if(partner.getPartnerCode().equals(""))
            throw BusinessException.generalError();

        kongService.createPartnerConsumer(partner);
        partnerRepository.save(partner);

        return BaseResponse.createSuccessResponseWithDoublePayload(
                "info","success registration",
                "data",partner);
    }

    private String getRandomCode(String email){
        String subEmail = email.substring(0,5);
        subEmail = subEmail.replace("@","a");

        Boolean stopper = true;
        Integer i = 0;
        String partnerCode = "";

        while (stopper){
            String uniqeCode = getRandomNumber(subEmail);
            Optional<Partner> optPartnerCode = partnerRepository.findByPartnerCode(uniqeCode);
            if(optPartnerCode.isPresent())
                i++;
            else{
                stopper = false;
                partnerCode = uniqeCode;
            }
            if(i>10) stopper = false;
        }
        return partnerCode.toUpperCase();
    }

    private String getRandomNumber(String subEmail) {
        Random r = new Random();
        String id = String.format("%04d", r.nextInt(10000));
        return subEmail+id;
    }

    String getRandomApiKey()
    {
        return UUID.randomUUID().toString();
    }

    public BaseResponse getToken(String partnerCode) {
        KongBaseResponse jwtInfoBase = kongService.getJwtInfo(partnerCode);
        KongDataResponse jwtInfo = jwtInfoBase.getData().get(0);

        return BaseResponse.createSuccessResponseWithSinglePayload(
                "token",
                generateTokenJWT(jwtInfo.getKey(),jwtInfo.getSecret()));
    }

    public BaseResponse getRefreshToken(String partnerCode, String consumerId) {
        Optional<Partner> optPartner = partnerRepository.findByPartnerCode(partnerCode);
        optPartner.orElseThrow(BusinessException::dataNotFound);
        Partner partner = optPartner.get();

        return BaseResponse.createSuccessResponseWithSinglePayload(
                "token",
                generateTokenJWT(consumerId,partner.getSecret()));
    }

    String generateTokenJWT(String consumerId, String secret){
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        Map<String,Object> map = new HashMap<>();
        map.put("typ","JWT");
        map.put(JWT_KEY_ID,consumerId);
        Key hmacKey = new SecretKeySpec((secret).getBytes(),
                SignatureAlgorithm.HS256.getJcaName());
        JwtBuilder builder = Jwts.builder()
                .setHeader(map)
                .setIssuedAt(now)
                .signWith(signatureAlgorithm, hmacKey)
                .setExpiration(new Date(nowMillis+JWT_EXPIRY));

        return builder.compact();
    }

    public BaseResponse getInfo(String partnerCode) {
        Optional<Partner> optPartner = partnerRepository.findByPartnerCode(partnerCode);
        optPartner.orElseThrow(BusinessException::dataNotFound);
        Partner partner = optPartner.get();

        KongDataResponse jwtInfo = kongService.getJwtInfo(partnerCode).getData().get(0);
        KongDataResponse keyInfo = kongService.getApiKeyInfo(partnerCode).getData().get(0);

        partner.setApiKey(keyInfo.getKey());

        return BaseResponse.createSuccessResponse(partner);
    }

    public BaseResponse processUpdateBalance(PartnerBalanceRequest balance) {
        Optional<Partner> optPartner = partnerRepository.findByPartnerCode(balance.getPartnerCode());
        optPartner.orElseThrow(BusinessException::dataNotFound);
        Partner partner = optPartner.get();
        partner.setBalance(partner.getBalance()+ balance.getBalance());
        partnerRepository.save(partner);
        return BaseResponse.createSuccessResponse();
    }
}