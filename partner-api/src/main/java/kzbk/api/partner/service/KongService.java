package kzbk.api.partner.service;

import kzbk.api.partner.config.KongConfig;
import kzbk.api.partner.model.dto.kong.ConsumerResponse;
import kzbk.api.partner.model.dto.kong.KongDataResponse;
import kzbk.api.partner.model.dto.kong.KongBaseResponse;
import kzbk.common.entity.Partner;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class KongService {

    private final KongConfig kongConfig;
    private final RestTemplate restTemplate;

    public Partner createPartnerConsumer(Partner partner){
        createConsumer(partner);
        createApiKey(partner);
        createJWTsecret(partner);
        return partner;
    }

    private void createJWTsecret(Partner partner) {
        Map<String, Object> outterMap = new HashMap<>();
        outterMap.put("secret",partner.getSecret());
        outterMap.put("algorithm", "HS256");

        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(outterMap);
        String url = kongConfig.getHost() + kongConfig.getApiConsumer() + partner.getPartnerCode() + kongConfig.getApiConsumerJwt();
        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(url).build(true);

        restTemplate.exchange(uriComponents.toUri(), HttpMethod.POST, entity, ConsumerResponse.class).getBody();
    }

    private void createConsumer(Partner partner) {
        Map<String, Object> outterMap = new HashMap<>();
        outterMap.put("username",partner.getPartnerCode());
        outterMap.put("tags", new String[]{"kzbk-user"});

        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(outterMap);
        String url = kongConfig.getHost() + kongConfig.getApiConsumer();
        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(url).build(true);

        restTemplate.exchange(uriComponents.toUri(), HttpMethod.POST, entity, ConsumerResponse.class).getBody();
    }

    private void createApiKey(Partner partner) {
        Map<String, Object> outterMap = new HashMap<>();
        outterMap.put("key",partner.getApiKey());

        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(outterMap);
        String url = kongConfig.getHost() + kongConfig.getApiConsumer() + partner.getPartnerCode() + kongConfig.getApiConsumerKey();
        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(url).build(true);

        restTemplate.exchange(uriComponents.toUri(), HttpMethod.POST, entity, ConsumerResponse.class).getBody();
    }

    public KongBaseResponse getJwtInfo(String partnerCode) {

        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(new HashMap<>());
        String url = kongConfig.getHost() + kongConfig.getApiConsumer() + partnerCode + kongConfig.getApiConsumerJwt();
        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(url).build(true);

        KongBaseResponse ok =
                restTemplate.exchange(uriComponents.toUri(), HttpMethod.GET, entity, KongBaseResponse.class).getBody();

        return ok;
    }

    public KongBaseResponse getApiKeyInfo(String partnerCode) {
        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(new HashMap<>());
        String url = kongConfig.getHost() + kongConfig.getApiConsumer() + partnerCode + kongConfig.getApiConsumerKey();
        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(url).build(true);

        return restTemplate.exchange(uriComponents.toUri(), HttpMethod.GET, entity, KongBaseResponse.class).getBody();
    }
}