package kzbk.api.transaction;

import kzbk.api.transaction.drools.dto.CashbackTransactionImpl;
import kzbk.api.transaction.drools.enums.RewardClassification;
import kzbk.api.transaction.drools.service.CashbackService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.util.Assert;

@SpringBootTest
@Slf4j
@ActiveProfiles("test")
class DroolsDemoApplicationTests {

    @Autowired
    CashbackService cashbackService;

    @Test
    void rulesTest_level_1() {
        CashbackTransactionImpl dt = CashbackTransactionImpl.builder()
                .partnerId("1001")
                .total(100000L)
                .couponCode("DUMMYCODEPROMO")
                .qty(1)
                .level(RewardClassification.BRONZE)
                .build();
        cashbackService.generateCashback(dt);
        logger(dt);
        Assert.isTrue(dt.getStarPoint().equals(1200),"Actual summary is not equal to expected summary");
    }
    @Test
    void rulesTest_level_2() {
        CashbackTransactionImpl dt = CashbackTransactionImpl.builder()
                .partnerId("1001")
                .total(500000L)
                .couponCode("DUMMYCODEPROMO")
                .qty(1)
                .level(RewardClassification.BRONZE)
                .build();
        cashbackService.generateCashback(dt);
        logger(dt);
        Assert.isTrue(dt.getStarPoint().equals(8750),"Actual summary is not equal to expected summary");
    }
    @Test
    void rulesTest_level_3() {
        CashbackTransactionImpl dt = CashbackTransactionImpl.builder()
                .partnerId("1001")
                .total(1000000L)
                .couponCode("DUMMYCODEPROMO")
                .qty(1)
                .level(RewardClassification.BRONZE)
                .build();
        cashbackService.generateCashback(dt);
        logger(dt);
        Assert.isTrue(dt.getStarPoint().equals(23000),"Actual summary is not equal to expected summary");
    }
    @Test
    void rulesTest_level_4() {
        CashbackTransactionImpl dt = CashbackTransactionImpl.builder()
                .partnerId("1001")
                .total(1000000L)
                .couponCode("DUMMYCODEPROMO")
                .qty(2)
                .level(RewardClassification.BRONZE)
                .build();
        cashbackService.generateCashback(dt);
        logger(dt);
        Assert.isTrue(dt.getStarPoint().equals(24500),"Actual summary is not equal to expected summary");
    }
    @Test
    void rulesTest_level_5() {
        CashbackTransactionImpl dt = CashbackTransactionImpl.builder()
                .partnerId("1001")
                .total(1500000L)
                .couponCode("DUMMYCODEPROMO")
                .qty(2)
                .level(RewardClassification.BRONZE)
                .build();
        cashbackService.generateCashback(dt);
        logger(dt);
        Assert.isTrue(dt.getStarPoint().equals(41250),"Actual summary is not equal to expected summary");
    }
    @Test
    void rulesTest_level_6() {
        CashbackTransactionImpl dt = CashbackTransactionImpl.builder()
                .partnerId("1001")
                .total(999999999L)
                .couponCode("DUMMYCODEPROMO")
                .qty(2)
                .level(RewardClassification.BRONZE)
                .build();
        cashbackService.generateCashback(dt);
        logger(dt);
        Assert.isTrue(dt.getStarPoint().equals(29500000),"Actual summary is not equal to expected summary");
    }
    @Test
    void rulesTest_level_10() {
        CashbackTransactionImpl dt = CashbackTransactionImpl.builder()
                .partnerId("1001")
                .total(999999999L)
                .couponCode("DUMMYCODEPROMO")
                .qty(3)
                .level(RewardClassification.BRONZE)
                .build();
        cashbackService.generateCashback(dt);
        logger(dt);
        Assert.isTrue(dt.getStarPoint().equals(33500000),"Actual summary is not equal to expected summary");
    }

    @Test
    void rulesTest_loyalty_bronze() {
        CashbackTransactionImpl dt = CashbackTransactionImpl.builder()
                .partnerId("1001")
                .total(99999999L)
                .couponCode("DUMMYCODEPROMO")
                .qty(4)
                .level(RewardClassification.BRONZE)
                .build();
        cashbackService.generateCashback(dt);
        logger(dt);
        Assert.isTrue(dt.getLoyaltyPoint().equals(15000),"loyalty point not expected");
    }

    @Test
    void rulesTest_loyalty_bronze1() {
        CashbackTransactionImpl dt = CashbackTransactionImpl.builder()
                .partnerId("1001")
                .total(999999999L)
                .couponCode("DUMMYCODEPROMO")
                .qty(7)
                .level(RewardClassification.BRONZE)
                .build();
        cashbackService.generateCashback(dt);
        logger(dt);
        Assert.isTrue(dt.getLoyaltyPoint().equals(25000),"loyalty point not expected");
    }

    @Test
    void rulesTest_loyalty_bronze2() {
        CashbackTransactionImpl dt = CashbackTransactionImpl.builder()
                .partnerId("1001")
                .total(999999999L)
                .couponCode("DUMMYCODEPROMO")
                .qty(10)
                .level(RewardClassification.BRONZE)
                .build();
        cashbackService.generateCashback(dt);
        logger(dt);
        Assert.isTrue(dt.getLoyaltyPoint().equals(35000),"loyalty point not expected");
    }

    @Test
    void rulesTest_loyalty_bronzenegatif() {
        CashbackTransactionImpl dt = CashbackTransactionImpl.builder()
                .partnerId("1001")
                .total(999999999L)
                .couponCode("DUMMYCODEPROMO")
                .qty(1)
                .level(RewardClassification.BRONZE)
                .build();
        cashbackService.generateCashback(dt);
        logger(dt);
        Assert.isTrue(dt.getLoyaltyPoint().equals(0),"loyalty point not expected");
    }

    void logger(CashbackTransactionImpl dt){
        log.info("total : {} , qty : {} , leve : {} , starpoin : {}",
                dt.getTotal(),dt.getQty(),dt.getLevel(),dt.getStarPoint());
    }
}