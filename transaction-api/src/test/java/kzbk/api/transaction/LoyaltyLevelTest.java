package kzbk.api.transaction;

import kzbk.api.transaction.drools.entity.CustomerTier;
import kzbk.api.transaction.drools.enums.RewardClassification;
import kzbk.api.transaction.drools.repository.CustomerTierRepository;
import kzbk.api.transaction.drools.repository.RewardRuleRepository;
import kzbk.api.transaction.drools.service.LoyaltyService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static kzbk.api.transaction.drools.enums.RewardClassification.*;


@SpringBootTest
@Slf4j
public class LoyaltyLevelTest {

    @Autowired
    RewardRuleRepository repository;

    @Autowired
    LoyaltyService loyaltyService;

    @Autowired
    CustomerTierRepository customerTierRepository;

    @Test
    @Modifying
    void testLoyaltyLevel(){
        customerTierRepository.deleteAll();
        Assert.isTrue(BRONZE.equals(getLevel()),"update level not pass");
        Assert.isTrue(BRONZE.equals(getLevel()),"update level not pass");
        Assert.isTrue(BRONZE.equals(getLevel()),"update level not pass");
        Assert.isTrue(BRONZE.equals(getLevel()),"update level not pass");
        Assert.isTrue(BRONZE.equals(getLevel()),"update level not pass");
        Assert.isTrue(BRONZE.equals(getLevel()),"update level not pass");
        Assert.isTrue(BRONZE.equals(getLevel()),"update level not pass");
        Assert.isTrue(SILVER.equals(getLevel()),"update level not pass");
        Assert.isTrue(SILVER.equals(getLevel()),"update level not pass");
        Assert.isTrue(SILVER.equals(getLevel()),"update level not pass");
        Assert.isTrue(SILVER.equals(getLevel()),"update level not pass");
        Assert.isTrue(SILVER.equals(getLevel()),"update level not pass");
        Assert.isTrue(SILVER.equals(getLevel()),"update level not pass");
        Assert.isTrue(SILVER.equals(getLevel()),"update level not pass");
        Assert.isTrue(GOLD.equals(getLevel()),"update level not pass");
        Assert.isTrue(GOLD.equals(getLevel()),"update level not pass");
        Assert.isTrue(GOLD.equals(getLevel()),"update level not pass");
        Assert.isTrue(GOLD.equals(getLevel()),"update level not pass");
        Assert.isTrue(GOLD.equals(getLevel()),"update level not pass");
        Assert.isTrue(GOLD.equals(getLevel()),"update level not pass");
        Assert.isTrue(GOLD.equals(getLevel()),"update level not pass");
        Assert.isTrue(GOLD.equals(getLevel()),"update level not pass");
        customerTierRepository.deleteAll();
    }

    @Test
    @Modifying
    void testLoyaltyLevelLast30Day(){
        LocalDate monthAgo = LocalDate.now().minusDays(31);
        Date date = Date.from(monthAgo.atStartOfDay(ZoneId.systemDefault()).toInstant());

        CustomerTier customerTier = CustomerTier.builder()
                .level(SILVER)
                .lastTransactionDate(date)
                .customerId(1)
                .totalTrx(4)
                .build();
        customerTierRepository.save(customerTier);
        Assert.isTrue(BRONZE.equals(getLevel()),"update level not pass");
        customerTierRepository.deleteAll();
    }

    RewardClassification getLevel(){
        RewardClassification level = loyaltyService.updateTierLevel("agusdwi89@gmail.com","BLSAF5525");
        log.info(level.toString());
        return level;
    }
}
