package kzbk.api.transaction;

import kzbk.api.transaction.drools.dto.CashbackTransactionImpl;
import kzbk.api.transaction.drools.enums.RewardClassification;
import kzbk.api.transaction.drools.service.CashbackService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

@SpringBootTest
class DroolsSilverGoldTests {

    @Autowired
    CashbackService cashbackService;

    @Test
    void rulesTest_loyalty_silver() {
        CashbackTransactionImpl dt = CashbackTransactionImpl.builder()
                .partnerId("1001")
                .total(9999999999999L)
                .couponCode("DUMMYCODEPROMO")
                .qty(4)
                .level(RewardClassification.SILVER)
                .build();
        cashbackService.generateCashback(dt);
        Assert.isTrue(dt.getLoyaltyPoint().equals(17000),"loyalty point not expected");
    }

    @Test
    void rulesTest_loyalty_silver1() {
        CashbackTransactionImpl dt = CashbackTransactionImpl.builder()
                .partnerId("1001")
                .total(9999999999999L)
                .couponCode("DUMMYCODEPROMO")
                .qty(7)
                .level(RewardClassification.SILVER)
                .build();
        cashbackService.generateCashback(dt);
        Assert.isTrue(dt.getLoyaltyPoint().equals(28500),"loyalty point not expected");
    }

    @Test
    void rulesTest_loyalty_silver2() {
        CashbackTransactionImpl dt = CashbackTransactionImpl.builder()
                .partnerId("1001")
                .total(9999999999999L)
                .couponCode("DUMMYCODEPROMO")
                .qty(10)
                .level(RewardClassification.SILVER)
                .build();
        cashbackService.generateCashback(dt);
        Assert.isTrue(dt.getLoyaltyPoint().equals(37500),"loyalty point not expected");
    }

    @Test
    void rulesTest_loyalty_silvernegatif() {
        CashbackTransactionImpl dt = CashbackTransactionImpl.builder()
                .partnerId("1001")
                .total(9999999999999L)
                .couponCode("DUMMYCODEPROMO")
                .qty(1)
                .level(RewardClassification.SILVER)
                .build();
        cashbackService.generateCashback(dt);
        Assert.isTrue(dt.getLoyaltyPoint().equals(0),"loyalty point not expected");
    }

    @Test
    void rulesTest_loyalty_gold() {
        CashbackTransactionImpl dt = CashbackTransactionImpl.builder()
                .partnerId("1001")
                .total(9999999999999L)
                .couponCode("DUMMYCODEPROMO")
                .qty(4)
                .level(RewardClassification.GOLD)
                .build();
        cashbackService.generateCashback(dt);
        Assert.isTrue(dt.getLoyaltyPoint().equals(27000),"loyalty point not expected");
    }

    @Test
    void rulesTest_loyalty_gold1() {
        CashbackTransactionImpl dt = CashbackTransactionImpl.builder()
                .partnerId("1001")
                .total(9999999999999L)
                .couponCode("DUMMYCODEPROMO")
                .qty(7)
                .level(RewardClassification.GOLD)
                .build();
        cashbackService.generateCashback(dt);
        Assert.isTrue(dt.getLoyaltyPoint().equals(38500),"loyalty point not expected");
    }

    @Test
    void rulesTest_loyalty_gold2() {
        CashbackTransactionImpl dt = CashbackTransactionImpl.builder()
                .partnerId("1001")
                .total(9999999999999L)
                .couponCode("DUMMYCODEPROMO")
                .qty(10)
                .level(RewardClassification.GOLD)
                .build();
        cashbackService.generateCashback(dt);
        Assert.isTrue(dt.getLoyaltyPoint().equals(47500),"loyalty point not expected");
    }

    @Test
    void rulesTest_loyalty_goldnegatif() {
        CashbackTransactionImpl dt = CashbackTransactionImpl.builder()
                .partnerId("1001")
                .total(9999999999999L)
                .couponCode("DUMMYCODEPROMO")
                .qty(1)
                .level(RewardClassification.GOLD)
                .build();
        cashbackService.generateCashback(dt);
        Assert.isTrue(dt.getLoyaltyPoint().equals(0),"loyalty point not expected");
    }
}