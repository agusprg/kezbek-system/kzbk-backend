package kzbk.api.transaction.drools.dto;
import kzbk.api.transaction.drools.enums.RewardClassification;


public interface CashbackTransaction {
    void setTransactionId(String transactionId);
    void setPartnerId(String partnerId);
    void setQty(Integer qty);
    void setTotal(Long total);
    void setCouponCode(String coupon);
    void setLevel(RewardClassification level);
    Integer getStarPoint();
    Integer getLoyaltyPoint();
}
