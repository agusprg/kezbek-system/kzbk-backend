package kzbk.api.transaction.drools.config;

import kzbk.api.transaction.drools.dto.CashbackTransactionImpl;
import kzbk.api.transaction.drools.entity.RewardRule;
import kzbk.api.transaction.drools.entity.RewardRuleDetail;
import kzbk.api.transaction.drools.entity.RewardRuleTier;
import kzbk.api.transaction.drools.entity.RewardRuleTierLoyalty;
import kzbk.api.transaction.drools.enums.RewardClassification;
import kzbk.api.transaction.drools.enums.RuleTierType;
import kzbk.api.transaction.drools.enums.ValueType;
import kzbk.api.transaction.drools.repository.RewardRuleDetailRepository;
import kzbk.api.transaction.drools.repository.RewardRuleRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.drools.model.Rule;
import org.drools.model.Variable;
import org.drools.model.impl.ModelImpl;
import org.drools.model.impl.RuleBuilder;
import org.drools.modelcompiler.builder.KieBaseBuilder;
import org.kie.api.KieBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.*;

import static org.drools.model.DSL.declarationOf;
import static org.drools.model.DSL.on;
import static org.drools.model.PatternDSL.*;


@Slf4j
@Configuration
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RuleEngineConfig {

    private final RewardRuleRepository rewardRuleRepository;
    private final RewardRuleDetailRepository rewardRuleDetailRepository;

    @Bean
    public KieBase kieBase() {
        ModelImpl model = new ModelImpl();
        List<Rule> cashBackRules = generateCashbackTierRules();
        for (Rule r : cashBackRules){
            model.addRule(r);
        }
        return KieBaseBuilder.createKieBaseFromModel(model);
    }

    private List<Rule> generateCashbackTierRules() {
        List<Rule> cashBackRules = new ArrayList<>();
        List<RewardRule> rewardRules = rewardRuleRepository.findAllByRuleEnabled();
        log.info("[REWARD KEZBEK, RULE-CONFIG]: {} rules found", rewardRules.size());
        Variable<CashbackTransactionImpl> transactionVariable = declarationOf(CashbackTransactionImpl.class);
        for(RewardRule rewardRule : rewardRules) {

            List<RewardRuleDetail> rewardRuleDetails = rewardRuleDetailRepository
                    .findById_RuleIdAndEnabled(rewardRule.getRuleId(), true);
            for (RewardRuleDetail rewardRuleDetail : rewardRuleDetails) {
                if(ValueType.TIER.equals(rewardRuleDetail.getValueType())) {
                    Variable<RewardRule> ruleVariable = declarationOf(RewardRule.class, from(rewardRule));
                    cashBackRules.addAll(addTierRules(rewardRule, rewardRuleDetail, transactionVariable, ruleVariable));
                }
                if(ValueType.TIER_LOYALTY.equals(rewardRuleDetail.getValueType())) {
                    Variable<RewardRule> ruleVariable = declarationOf(RewardRule.class, from(rewardRule));
                    cashBackRules.addAll(addTierLoyaltyRules(rewardRule, rewardRuleDetail, transactionVariable, ruleVariable));
                }
            }
            log.info("[REWARD KEZBEK,RULE-CONFIG]: {} rule details found", rewardRuleDetails.size());
        }
        return cashBackRules;
    }

    private List<Rule> addTierLoyaltyRules(RewardRule rewardRule, RewardRuleDetail ruleDetail, Variable<CashbackTransactionImpl> transactionVariable, Variable<RewardRule> ruleVariable) {
        List<Rule> newTierRules = new ArrayList<>();
        Set<RewardRuleTierLoyalty> rewardRuleTierLoyalties = ruleDetail.getRuleTierLoyalties();
        for (RewardRuleTierLoyalty rewardRuleTierLoyalty : rewardRuleTierLoyalties) {
            Variable<RewardRuleTierLoyalty> tierVariable = declarationOf(RewardRuleTierLoyalty.class, from(rewardRuleTierLoyalty));
            RuleBuilder ruleBuilder = rule(rewardRuleTierLoyalty.getRuleId() + "-" + rewardRule.getRuleName() + "-" + rewardRuleTierLoyalty.getRuleItemCode() + "-" + rewardRuleTierLoyalty.getTierLoyaltyId());
            Calendar startCal = Calendar.getInstance();
            Calendar endCal = Calendar.getInstance();
            startCal.setTime(rewardRule.getStartDate());
            endCal.setTime(rewardRule.getEndDate());

            ruleBuilder.attribute(Rule.Attribute.DATE_EFFECTIVE, startCal);
            ruleBuilder.attribute(Rule.Attribute.DATE_EXPIRES, endCal);
            Variable<String> partnerVariable = declarationOf(String.class, "partnerId");
            Variable<RewardClassification> rewardClassification = declarationOf(RewardClassification.class, "classification");

            Rule rule = ruleBuilder.build(
                    pattern(ruleVariable).bind(partnerVariable, RewardRule::getPartnerId),
                    pattern(tierVariable).bind(tierVariable, t -> t),
                    pattern(transactionVariable).expr("partner-level-pattern",partnerVariable, (o,partner) ->
                            (partner == null || Objects.equals(o.getPartnerId(), partner))
                    ),
                    pattern(transactionVariable).expr("tier-pattern", ruleVariable, tierVariable, (trx, rr, tier) -> {
                        return tier.getMinQty() <= trx.getQty()
                                && tier.getMaxQty() >= trx.getQty()
                                && tier.getLevel().equals(trx.getLevel())
                                && Objects.equals(trx.getCouponCode(), tier.getRuleItemCode());
                    }),
                    on(transactionVariable, tierVariable).breaking().execute((t, tr) -> {
                        if(RuleTierType.PERCENTAGE.equals(tr.getTierType())){
                            t.setLoyaltyPoint(Math.toIntExact(tr.getStarValue()*t.getTotal()/100));
                        }else if(RuleTierType.FIXED.equals(tr.getTierType())){
                            t.setLoyaltyPoint(Math.toIntExact(tr.getStarValue()));
                        }
                    })
            );
            newTierRules.add(rule);
        }
        return newTierRules;
    }

    private List<Rule> addTierRules(RewardRule rewardRule, RewardRuleDetail ruleDetail, Variable<CashbackTransactionImpl> transactionVariable, Variable<RewardRule> ruleVariable) {
        List<Rule> newTierRules = new ArrayList<>();
        List<RewardRuleTier> rewardRuleTiers = ruleDetail.getRuleTiers();

        for (RewardRuleTier rewardRuleTier : rewardRuleTiers) {
            Variable<RewardRuleTier> tierVariable = declarationOf(RewardRuleTier.class, from(rewardRuleTier));
            RuleBuilder ruleBuilder = rule(rewardRuleTier.getRuleId() + "-" + rewardRule.getRuleName() + "-" + rewardRuleTier.getRuleItemCode() + "-" + rewardRuleTier.getTierId());
            Calendar startCal = Calendar.getInstance();
            Calendar endCal = Calendar.getInstance();
            startCal.setTime(rewardRule.getStartDate());
            endCal.setTime(rewardRule.getEndDate());

            ruleBuilder.attribute(Rule.Attribute.DATE_EFFECTIVE, startCal);
            ruleBuilder.attribute(Rule.Attribute.DATE_EXPIRES, endCal);
            Variable<String> partnerVariable = declarationOf(String.class, "partnerId");

            Rule rule = ruleBuilder.build(
                    pattern(ruleVariable).bind(partnerVariable, RewardRule::getPartnerId),
                    pattern(tierVariable).bind(tierVariable, t -> t),
                    pattern(transactionVariable).expr("partner-pattern",partnerVariable, (o,partner) ->
                            (partner == null || Objects.equals(o.getPartnerId(), partner))
                    ),
                    pattern(transactionVariable).expr("tier-pattern", ruleVariable, tierVariable, (trx, rr, tier) -> {
                        return tier.getMinQty() <= trx.getQty() && tier.getMaxQty() >= trx.getQty()
                                && tier.getMinTotal() <= trx.getTotal() && tier.getMaxTotal() >= trx.getTotal()
                                && Objects.equals(trx.getCouponCode(), tier.getRuleItemCode());
                    }),
                    on(transactionVariable, tierVariable).breaking().execute((t, tr) -> {
                        if(RuleTierType.PERCENTAGE.equals(tr.getTierType())){
                            t.setStarPoint(Math.toIntExact(Math.round( (tr.getStarValue()*t.getTotal()/100))));
                        }else if(RuleTierType.FIXED.equals(tr.getTierType())){
                            t.setStarPoint(Math.toIntExact(Math.round(tr.getStarValue())));
                        }
                    })
            );
            newTierRules.add(rule);
        }
        return newTierRules;
    }
}