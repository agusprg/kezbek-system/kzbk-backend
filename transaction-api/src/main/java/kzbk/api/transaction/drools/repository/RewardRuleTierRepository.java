package kzbk.api.transaction.drools.repository;



import kzbk.api.transaction.drools.entity.RewardRuleTier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RewardRuleTierRepository extends JpaRepository<RewardRuleTier, Long> { }
