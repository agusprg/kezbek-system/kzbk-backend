package kzbk.api.transaction.drools.entity;

import kzbk.api.transaction.drools.enums.RuleTierType;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
public class RewardRuleTier implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_REWARD_RULE_TIER")
    @SequenceGenerator(name="SEQ_REWARD_RULE_TIER", sequenceName = "SEQ_REWARD_RULE_TIER", allocationSize=1, initialValue = 1)
    private Long tierId;

    @Column(name = "RULE_ID")
    private Long ruleId;

    @Column(name = "RULE_ITEM_CODE")
    private String ruleItemCode;

    @Enumerated(EnumType.STRING)
    private RuleTierType tierType;

    private Integer minQty;
    private Integer maxQty;
    private Long minTotal;
    private Long maxTotal;
    private Double starValue;
}