package kzbk.api.transaction.drools.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import kzbk.api.transaction.drools.enums.RewardClassification;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CashbackTransactionImpl implements CashbackTransaction{
    private String transactionId;
    private String partnerId;
    private Integer qty;
    private Long total;
    private String couponCode;
    private Integer starPoint;
    private Integer loyaltyPoint;
    private RewardClassification level;
}
