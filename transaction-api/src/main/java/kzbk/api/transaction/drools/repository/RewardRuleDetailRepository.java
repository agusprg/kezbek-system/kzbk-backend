package kzbk.api.transaction.drools.repository;


import kzbk.api.transaction.drools.entity.RewardRuleDetail;
import kzbk.api.transaction.drools.entity.RewardRuleDetailId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RewardRuleDetailRepository extends JpaRepository<RewardRuleDetail, RewardRuleDetailId> {
    List<RewardRuleDetail> findById_RuleIdAndEnabled(Long ruleId, Boolean enabled);
}
