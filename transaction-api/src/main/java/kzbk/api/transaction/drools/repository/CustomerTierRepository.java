package kzbk.api.transaction.drools.repository;

import kzbk.api.transaction.drools.entity.CustomerTier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface CustomerTierRepository extends JpaRepository<CustomerTier, Integer> {

    @Query("SELECT c FROM CustomerTier c WHERE c.customerId = :id and c.lastTransactionDate >= :monthAgo" +
            " order by c.lastTransactionDate desc ")
    List<CustomerTier> findByCustomerIdAndDateAfter(Integer id, Date monthAgo);

    @Query("SELECT c FROM CustomerTier c WHERE c.email = :email and c.partnerCode = :partnerCode " +
            " and c.lastTransactionDate >= :monthAgo order by c.lastTransactionDate desc ")
    List<CustomerTier> findByCustomerAndDateAfter(String email,String partnerCode, Date monthAgo);
}
