package kzbk.api.transaction.drools.enums;

public enum RewardClassification {
    GOLD,
    SILVER,
    BRONZE
}
