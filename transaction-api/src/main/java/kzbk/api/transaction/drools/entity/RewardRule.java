package kzbk.api.transaction.drools.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RewardRule implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_REWARD_RULE")
    @SequenceGenerator(name="SEQ_REWARD_RULE", sequenceName = "SEQ_REWARD_RULE", allocationSize=1, initialValue = 1)
    @Column(name = "RULE_ID")
    private Long ruleId;

    private String partnerId;
    private String ruleName;
    private Date startDate;
    private Date endDate;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "RULE_ID", insertable = false, updatable = false)
    private Set<RewardRuleDetail> ruleDetails;
}