package kzbk.api.transaction.drools.entity;

import kzbk.api.transaction.drools.enums.ValueType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "REWARD_RULE_DETAIL")
public class RewardRuleDetail {
    @EmbeddedId
    private RewardRuleDetailId id;

    @Enumerated(EnumType.STRING)
    private ValueType valueType;
    private Boolean enabled;

    @OneToMany(fetch = FetchType.EAGER)
    @OrderBy("tierId ASC")
    @JoinColumns({
            @JoinColumn(name = "RULE_ID", insertable = false, updatable = false),
            @JoinColumn(name = "RULE_ITEM_CODE", insertable = false, updatable = false)
    })
    private List<RewardRuleTier> ruleTiers;

    @OneToMany(fetch = FetchType.EAGER)
    @OrderBy("tierLoyaltyId ASC")
    @JoinColumns({
            @JoinColumn(name = "RULE_ID", insertable = false, updatable = false),
            @JoinColumn(name = "RULE_ITEM_CODE", insertable = false, updatable = false)
    })
    private Set<RewardRuleTierLoyalty> ruleTierLoyalties;
}
