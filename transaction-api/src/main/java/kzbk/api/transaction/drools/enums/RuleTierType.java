package kzbk.api.transaction.drools.enums;

public enum RuleTierType {
    FIXED,
    PERCENTAGE
}
