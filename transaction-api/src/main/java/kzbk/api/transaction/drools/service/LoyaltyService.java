package kzbk.api.transaction.drools.service;

import kzbk.api.transaction.drools.entity.CustomerTier;
import kzbk.api.transaction.drools.enums.RewardClassification;
import kzbk.api.transaction.drools.repository.CustomerTierRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import static kzbk.api.transaction.drools.enums.RewardClassification.*;


@Service
@Slf4j
public class LoyaltyService {

    @Autowired
    CustomerTierRepository repository;

    public RewardClassification getTierLevel(String email, String partnerCode){
        LocalDate monthAgo = LocalDate.now().minusDays(30);
        Date date = Date.from(monthAgo.atStartOfDay(ZoneId.systemDefault()).toInstant());

        List<CustomerTier> dataTier =  repository.findByCustomerAndDateAfter(email,partnerCode,date);

        return (dataTier.size()==0) ? BRONZE : dataTier.get(0).getLevel();
    }

    public RewardClassification updateTierLevel(String email, String partnerCode){
        LocalDate monthAgo = LocalDate.now().minusDays(30);
        Date date = Date.from(monthAgo.atStartOfDay(ZoneId.systemDefault()).toInstant());

        List<CustomerTier> dataTier =  repository.findByCustomerAndDateAfter(email,partnerCode,date);

        CustomerTier customerTier;
        if(dataTier.size()==0){
            customerTier = CustomerTier.builder()
                    .email(email)
                    .partnerCode(partnerCode)
                    .level(BRONZE)
                    .totalTrx(1)
                    .lastTransactionDate(new Date())
                    .build();
        }else{
            customerTier = dataTier.get(0);
            customerTier.setLastTransactionDate(new Date());
            getLevel(customerTier);
            repository.save(customerTier);
        }
        repository.save(customerTier);
        return customerTier.getLevel();
    }

    private void getLevel(CustomerTier customerTier) {
        Integer totalTrx = customerTier.getTotalTrx()+1;
        RewardClassification currentLevel = customerTier.getLevel();
        if(totalTrx > 7){
            switch(customerTier.getLevel()) {
                case BRONZE:
                    currentLevel = SILVER;
                    break;
                case SILVER:
                    currentLevel = GOLD;
                    break;
                case GOLD:
                    currentLevel = GOLD;
                    break;
                default:
                    currentLevel = BRONZE;
            }
            totalTrx = 1;
        }
        customerTier.setLevel(currentLevel);
        customerTier.setTotalTrx(totalTrx);
    }
}