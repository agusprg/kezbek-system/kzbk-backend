package kzbk.api.transaction.drools.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class RewardRuleDetailId implements Serializable {
    @Column(name = "RULE_ID")
    private Long ruleId;

    @Column(name = "RULE_ITEM_CODE")
    private String ruleItemCode;
}
