package kzbk.api.transaction.drools.repository;

import kzbk.api.transaction.drools.entity.RewardRule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface RewardRuleRepository extends JpaRepository<RewardRule, Long> {

    @Query("SELECT r FROM RewardRule r JOIN RewardRuleDetail rrd on r.ruleId = rrd.id.ruleId " +
            "WHERE rrd.enabled = true")
    List<RewardRule> findAllByRuleEnabled();

    @Modifying
    @Transactional
    @Query("DELETE FROM RewardRule r where r.ruleId in :id")
    void deleteAllById(List<Long> id);
}
