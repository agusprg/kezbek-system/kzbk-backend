package kzbk.api.transaction.drools.entity;

import kzbk.api.transaction.drools.enums.RewardClassification;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerTier implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CUSTOMER_TIER")
    @SequenceGenerator(name="SEQ_CUSTOMER_TIER", sequenceName = "SEQ_CUSTOMER_TIER", allocationSize=1, initialValue = 100)
    private Long id;

    private Integer customerId;
    private String partnerCode;
    private String email;
    private Date lastTransactionDate;
    private Integer totalTrx;

    @Enumerated(EnumType.STRING)
    private RewardClassification level;
}