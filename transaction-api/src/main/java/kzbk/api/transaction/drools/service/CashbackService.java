package kzbk.api.transaction.drools.service;


import kzbk.api.transaction.drools.config.TrackingAgendaEventListener;
import kzbk.api.transaction.drools.dto.CashbackTransactionImpl;
import kzbk.api.transaction.drools.repository.RewardRuleRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.kie.api.KieBase;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@Slf4j
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class CashbackService {

    final KieBase kieBase;
    final RewardRuleRepository rule;

    public CashbackTransactionImpl generateCashback(CashbackTransactionImpl cashbackTransactionImpl) {
        System.out.println(rule.findAll().size());

        cashbackTransactionImpl.setStarPoint(0);
        cashbackTransactionImpl.setLoyaltyPoint(0);
        KieSession kieSession = null;
        try {
            kieSession = kieBase.newKieSession();
            TrackingAgendaEventListener agendaEventListener = new TrackingAgendaEventListener();
            kieSession.addEventListener(agendaEventListener);
            log.info("[KEZBEK-RULE] trxType={}, trxId={} msg=processing Orbit Sellout reward",
                    cashbackTransactionImpl.getCouponCode(), cashbackTransactionImpl.getTransactionId());


            kieSession.insert(cashbackTransactionImpl);
            fireOrbitSelloutRules(cashbackTransactionImpl, kieSession);
            log.info("[KEZBEK-RULE] trxType={}, trxId={} Orbit Sellout rule match={}",
                    cashbackTransactionImpl.getCouponCode(), cashbackTransactionImpl.getTransactionId(), agendaEventListener.matchesToString(cashbackTransactionImpl.getTransactionId()));

            log.info("[KEZBEK-RULE] trxType={}, trxId={}, outletId={}, starPoint={}",
                    cashbackTransactionImpl.getCouponCode(), cashbackTransactionImpl.getTransactionId(), cashbackTransactionImpl.getStarPoint());
        } catch (Exception e) {
            log.error("[KEZBEK-RULE]", e);
        } finally {
            if (Objects.nonNull(kieSession)) {
                kieSession.dispose();
            }
        }
        return cashbackTransactionImpl;
    }

    private void fireOrbitSelloutRules(CashbackTransactionImpl cashbackTransactionImpl, KieSession kieSession) {
        int match = kieSession.fireAllRules();
        if (cashbackTransactionImpl.getStarPoint().equals(0) && match == 0) {
            log.info("[REWARD-ORBIT-SELLOUT] trxType={}, trxId={} msg=Orbit Sellout transaction does not match any rule in Parameter Set",
                    cashbackTransactionImpl.getCouponCode(), cashbackTransactionImpl.getTransactionId());
        } else {
            log.info("[REWARD-ORBIT-SELLOUT] trxType={}, trxId={} msg=Orbit Sellout transaction match rules in parameter set",
                    cashbackTransactionImpl.getCouponCode(), cashbackTransactionImpl.getTransactionId());
        }
    }
}
