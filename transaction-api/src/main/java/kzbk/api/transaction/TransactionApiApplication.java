package kzbk.api.transaction;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = {"kzbk.api.transaction","kzbk.common"})
@EnableJpaRepositories(basePackages = {"kzbk.common.repository","kzbk.api.transaction.drools.repository"})
@EntityScan(basePackages = {"kzbk.common.entity","kzbk.api.transaction.drools.entity"})
@ComponentScan(basePackages = {"kzbk.**"})
public class TransactionApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(TransactionApiApplication.class, args);
    }


}
