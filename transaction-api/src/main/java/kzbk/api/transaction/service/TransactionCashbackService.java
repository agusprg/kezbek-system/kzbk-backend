package kzbk.api.transaction.service;

import kzbk.api.transaction.drools.dto.CashbackTransactionImpl;
import kzbk.api.transaction.drools.enums.RewardClassification;
import kzbk.api.transaction.drools.service.CashbackService;
import kzbk.api.transaction.drools.service.LoyaltyService;
import kzbk.common.dto.response.CashbackResponse;
import kzbk.api.transaction.dto.TransactionCashbackRequest;
import kzbk.common.dto.response.TransactionCashbackResponse;
import kzbk.common.dto.response.BaseResponse;
import kzbk.common.entity.CashbackTransaction;
import kzbk.common.entity.Customer;
import kzbk.common.entity.CustomerPartnerId;
import kzbk.common.enums.TrxCashbackStatus;
import kzbk.common.helper.ObjectMapperUtils;
import kzbk.common.helper.TransactionHelper;
import kzbk.common.helper.exception.BusinessException;
import kzbk.common.repository.CashbackTransactionRepository;
import kzbk.common.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TransactionCashbackService {

    private final CustomerRepository customerRepository;
    private final CashbackTransactionRepository cashbackTransactionRepository;
    private final RabbitMqSender rabbitMqSender;
    private final CashbackService cashbackService;
    private final LoyaltyService loyaltyService;

    public BaseResponse getPotentialCashback(String partnerId, TransactionCashbackRequest trx) {

        validateCustomer(partnerId,trx);
        CashbackResponse cashback = getCashBackFromDrools(trx);

        TransactionCashbackResponse cashbackResponse = null;
        cashbackResponse = ObjectMapperUtils.map(trx,TransactionCashbackResponse.class);
        cashbackResponse.setCashback(cashback);

        return BaseResponse.createSuccessResponse(cashbackResponse);
    }

    public BaseResponse getCashback(String partnerId, TransactionCashbackRequest trx) {
        validateCustomer(partnerId,trx);
        CashbackResponse cashback = getCashBackFromDrools(trx);

        if(cashbackTransactionRepository.findByTransactionId(trx.getTransactionId()).isPresent())
            throw BusinessException.transactionIdalreadyExist();

        TransactionCashbackResponse cashbackResponse = null;
        cashbackResponse = ObjectMapperUtils.map(trx,TransactionCashbackResponse.class);
        cashbackResponse.setCashback(cashback);
        cashbackResponse.setStatus(TrxCashbackStatus.INIT);

        CashbackTransaction ctrx = CashbackTransaction.builder()
                .id(TransactionHelper.generateCzbkTrxId())
                .transactionId(trx.getTransactionId())
                .partnerId(partnerId)
                .email(trx.getEmail())
                .loyaltyCashback(cashback.getLoyaltyCashback())
                .shoppingCashback(cashback.getShoppingCashback())
                .status(TrxCashbackStatus.INIT)
                .total(trx.getTotal())
                .qty(trx.getQty())
                .level(cashback.getLevel())
                .build();
        cashbackTransactionRepository.save(ctrx);
        loyaltyService.updateTierLevel(trx.getEmail(),trx.getPartnerCode());
        cashbackResponse.setId(ctrx.getId());
        sendToQueue(cashbackResponse);
        cashbackResponse.setId(null);
        return BaseResponse.createSuccessResponse(cashbackResponse);
    }

    private void validateCustomer(String partnerId, TransactionCashbackRequest trx){
        if(!partnerId.equals(trx.getPartnerCode()))
            throw BusinessException.invalidPartner();

        CustomerPartnerId id = new CustomerPartnerId(partnerId,trx.getEmail());
        Optional<Customer> optCustomer = customerRepository.findById(id);
        if(!optCustomer.isPresent())
            throw BusinessException.customerNotRegistered();
    }

    private CashbackResponse getCashBackFromDrools(TransactionCashbackRequest trx){
        CashbackTransactionImpl cbl = CashbackTransactionImpl.builder()
                .couponCode(trx.getCouponCode())
                .level(loyaltyService.getTierLevel(trx.getEmail(),trx.getPartnerCode()))
                .qty(trx.getQty())
                .partnerId(trx.getPartnerCode())
                .total(trx.getTotal())
                .build();
        cashbackService.generateCashback(cbl);
        CashbackResponse cashback = CashbackResponse.builder()
                .loyaltyCashback(Long.valueOf(cbl.getLoyaltyPoint()))
                .shoppingCashback(Long.valueOf(cbl.getStarPoint()))
                .level(cbl.getLevel().toString())
                .build();
        return cashback;
    }

    private void sendToQueue(TransactionCashbackResponse cashbackResponse) {
        rabbitMqSender.send(cashbackResponse);
    }

    public BaseResponse getCashbackStatus(String partnerId, String transactionId) {
        CashbackTransaction history = cashbackTransactionRepository.findByTransactionId(transactionId)
                .orElseThrow(BusinessException::notFound);
        if(!history.getPartnerId().equals(partnerId))
            throw BusinessException.notFound();
        CashbackResponse cashback = CashbackResponse.builder()
                .loyaltyCashback(Long.valueOf(history.getLoyaltyCashback()))
                .shoppingCashback(Long.valueOf(history.getShoppingCashback()))
                .level(history.getLevel())
                .build();
        TransactionCashbackResponse cashbackResponse = new TransactionCashbackResponse(
                null, history.getTransactionId(),cashback,
                history.getEmail(), history.getQty(), history.getTotal(), history.getStatus());
        return BaseResponse.createSuccessResponse(cashbackResponse);
    }

    public BaseResponse getHistoryTransaction(String email, String partnerCode) {


        customerRepository.findById(new CustomerPartnerId(partnerCode,email))
                .orElseThrow(BusinessException::notFound);

        List<CashbackTransaction> transactionList = cashbackTransactionRepository
                .findAllByEmailAndPartnerId(email,partnerCode);
        return BaseResponse.createSuccessResponse(transactionList);
    }
}
