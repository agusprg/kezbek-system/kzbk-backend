package kzbk.api.transaction.service;

import kzbk.api.transaction.config.RabbitMQpropertyConfig;
import kzbk.common.dto.response.TransactionCashbackResponse;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RabbitMqSender {

    private RabbitTemplate rabbitTemplate;
    @Autowired
    public RabbitMqSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Autowired
    RabbitMQpropertyConfig config;

    public void send(TransactionCashbackResponse t){
        rabbitTemplate.convertAndSend(config.getTransactionExchange(),config.getTransactionRoutingkey(), t);
        rabbitTemplate.convertAndSend(config.getNotifyExchange(),config.getNotifyRoutingkey(), t);
    }
}
