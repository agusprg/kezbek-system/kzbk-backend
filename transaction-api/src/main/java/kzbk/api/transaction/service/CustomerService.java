package kzbk.api.transaction.service;

import kzbk.api.transaction.dto.CustomerObRequest;
import kzbk.common.dto.response.BaseResponse;
import kzbk.common.entity.Customer;
import kzbk.common.entity.CustomerPartnerId;
import kzbk.common.entity.Partner;
import kzbk.common.helper.exception.BusinessException;
import kzbk.common.repository.CustomerRepository;
import kzbk.common.repository.PartnerRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final PartnerRepository partnerRepository;

    public BaseResponse submitOnBoarding(String partnerId, CustomerObRequest customerRequest)  {
        if(!partnerId.equals(customerRequest.getPartnerCode()))
            throw BusinessException.invalidPartner();

        CustomerPartnerId id = new CustomerPartnerId(partnerId,customerRequest.getEmail());
        Optional<Customer> optCustomer = customerRepository.findById(id);
        if(optCustomer.isPresent())
            throw BusinessException.alreadyOnBoarding();

        Partner partner = partnerRepository.findByPartnerCode(customerRequest.getPartnerCode())
                .orElseThrow(BusinessException::invalidPartner);

        Customer customer = Customer.builder()
                .id(id)
                .name(customerRequest.getName())
                .emoneyWallet(partner.getEmoneyWallet())
                .emoneyNumber(customerRequest.getEmoneyNumber())
                .build();

        customerRepository.save(customer);
        return BaseResponse.createSuccessResponse();
    }

    public BaseResponse checkOnBoarding(String partnerId, String partnerCode, String email) {
        if(!partnerId.equals(partnerCode))
            throw BusinessException.invalidPartner();

        CustomerPartnerId id = new CustomerPartnerId(partnerId,email);
        Optional<Customer> optCustomer = customerRepository.findById(id);

        if(optCustomer.isPresent())
            return BaseResponse.createSuccessResponseWithDoublePayload(
                    "status","Registered",
                    "customer",optCustomer.get());
        else return BaseResponse.createFailedResponse(BusinessException.customerNotRegistered());
    }
}
