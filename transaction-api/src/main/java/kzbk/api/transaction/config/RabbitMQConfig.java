package kzbk.api.transaction.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Autowired
    RabbitMQpropertyConfig config;

    @Bean
    Queue queue() {
        return new Queue(config.getTransactionQueue(), true, false, false);
    }

    @Bean
    Queue notifyQueue() {
        return new Queue(config.getNotifyQueue(), true, false, false);
    }

    @Bean
    Exchange myExchange() {
        return ExchangeBuilder.directExchange(config.getTransactionExchange()).durable(true).build();
    }

    @Bean
    Exchange notifyMyExchange() {
        return ExchangeBuilder.directExchange(config.getNotifyExchange()).durable(true).build();
    }

    @Bean
    Binding binding() {
        return BindingBuilder
                .bind(queue())
                .to(myExchange())
                .with(config.getTransactionRoutingkey())
                .noargs();
    }

    @Bean
    Binding bindingNotify() {
        return BindingBuilder
                .bind(notifyQueue())
                .to(notifyMyExchange())
                .with(config.getNotifyRoutingkey())
                .noargs();
    }

    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory(config.getHost());
        cachingConnectionFactory.setUsername(config.getUsername());
        cachingConnectionFactory.setPassword(config.getPassword());
        return cachingConnectionFactory;
    }
    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }
}