package kzbk.api.transaction.controller;

import kzbk.api.transaction.dto.CustomerObRequest;
import kzbk.api.transaction.dto.TransactionCashbackRequest;
import kzbk.api.transaction.service.TransactionCashbackService;
import kzbk.common.dto.response.BaseResponse;
import kzbk.common.dto.response.TransactionCashbackResponse;
import kzbk.common.entity.CashbackTransaction;
import kzbk.common.helper.HeaderExtraction;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("transaction")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TransactionController {

    private final TransactionCashbackService transactionCashbackService;
    private final HeaderExtraction header;

    @PostMapping("potential")
    BaseResponse<TransactionCashbackResponse> getPotential(@Valid @RequestBody TransactionCashbackRequest trx, HttpServletRequest httpRequest){
        return transactionCashbackService.getPotentialCashback(header.getPartnerId(httpRequest),trx);
    }

    @PostMapping("cashback")
    BaseResponse<TransactionCashbackResponse> submitCashback(@Valid @RequestBody TransactionCashbackRequest trx,HttpServletRequest httpRequest){
        return transactionCashbackService.getCashback(header.getPartnerId(httpRequest),trx);
    }

    @GetMapping("status")
    BaseResponse<TransactionCashbackResponse> getStatus(@RequestParam(name = "transaction_id") String transactionId,HttpServletRequest httpRequest){
        return transactionCashbackService.getCashbackStatus(header.getPartnerId(httpRequest),transactionId);
    }

    @GetMapping("history")
    BaseResponse<List<CashbackTransaction>> historyCustomer(
            @RequestParam String email,
            @RequestParam(name = "partner_code") String partnerCode,
            HttpServletRequest httpRequest){
        return transactionCashbackService.getHistoryTransaction(email,partnerCode);
    }
}