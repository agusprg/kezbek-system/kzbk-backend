package kzbk.api.transaction.controller;

import kzbk.api.transaction.dto.CustomerObRequest;
import kzbk.api.transaction.service.CustomerService;
import kzbk.common.dto.response.BaseResponse;
import kzbk.common.helper.HeaderExtraction;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;


@RestController
@RequestMapping("customer")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CustomerController {

    private final CustomerService customerService;
    private final HeaderExtraction header;

    @PostMapping("onboarding")
    BaseResponse onboardingCustomer(@Valid @RequestBody CustomerObRequest customer, HttpServletRequest httpRequest) {
        return customerService.submitOnBoarding(header.getPartnerId(httpRequest),customer);
    }

    @GetMapping("onboarding/status")
    BaseResponse onboardingCustomer(
            @RequestParam String email,
            @RequestParam(name = "partner_code") String partnerCode,
            HttpServletRequest httpRequest) {
        return customerService.checkOnBoarding(header.getPartnerId(httpRequest),partnerCode,email);
    }

}