package kzbk.api.transaction.dto;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class TransactionCashbackRequest {

    @NotEmpty(message = "transaction id can't be empty")
    private String transactionId;
    @NotEmpty(message = "partner code can't be empty")
    private String partnerCode;
    @NotEmpty(message = "email can't be empty")
    private String email;
    @NotEmpty(message = "coupon code can't be empty")
    private String couponCode;
    @NotNull(message = "total can't be 0")
    @Min(1)
    private Integer qty;
    @NotNull(message = "total can't be 0")
    @Min(1)
    private Long total;
}
