package kzbk.api.transaction.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class CustomerObRequest {

    @NotEmpty(message = "partner code can't be empty")
    private String partnerCode;
    @NotEmpty(message = "customer name can't be empty")
    private String name;
    @NotEmpty(message = "customer email can't be empty")
    private String email;
    @NotEmpty(message = "customer asociated emoney number can't be empty")
    private String emoneyNumber;
}
